def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:2]=' + postag[:2],
        ]
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:word.isdigit=%s' % word1.isdigit(),
            '-1:postag=' + postag1,
            '-1:postag[:2]=' + postag1[:2],
            ])
    else:
        features.append('BOS')

    if i > 1:
        word2 = sent[i-2][0]
        postag2 = sent[i-2][1]
        features.extend([
            '-2:word.lower=' + word2.lower(),
            '-2:word.istitle=%s' % word2.istitle(),
            '-2:word.isupper=%s' % word2.isupper(),
            '-2:word.isdigit=%s' % word2.isdigit(),
            '-2:postag=' + postag2,
            '-2:postag[:2]=' + postag2[:2],
            ])
    
    if i < (len(sent)-2):
        word1 = sent[i+2][0]
        postag1 = sent[i+2][1]
        features.extend([
            '+2:word.lower=' + word1.lower(),
            '+2:word.istitle=%s' % word1.istitle(),
            '+2:word.isupper=%s' % word1.isupper(),
            '+2:word.isdigit=%s' % word1.isdigit(),           
            '+2:postag=' + postag1,
            '+2:postag[:2]=' + postag1[:2],
            ])

    if i < (len(sent)-1):
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:word.isdigit=%s' % word1.isdigit(),           
            '+1:postag=' + postag1,
            '+1:postag[:2]=' + postag1[:2],
            ])
    else:
        features.append('EOS')

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]

#Duyen ha

def word2features_level_2(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    pre_label= sent[i][2]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:2]=' + postag[:2],
        'pre_label=' + pre_label
        ]
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:word.isdigit=%s' % word1.isdigit(),
            '-1:postag=' + postag1,
            '-1:postag[:2]=' + postag1[:2],
            ])
    else:
        features.append('BOS')

    if i > 1:
        word2 = sent[i-2][0]
        postag2 = sent[i-2][1]
        features.extend([
            '-2:word.lower=' + word2.lower(),
            '-2:word.istitle=%s' % word2.istitle(),
            '-2:word.isupper=%s' % word2.isupper(),
            '-2:word.isdigit=%s' % word2.isdigit(),
            '-2:postag=' + postag2,
            '-2:postag[:2]=' + postag2[:2],
            ])
    
    if i < (len(sent)-2):
        word1 = sent[i+2][0]
        postag1 = sent[i+2][1]
        features.extend([
            '+2:word.lower=' + word1.lower(),
            '+2:word.istitle=%s' % word1.istitle(),
            '+2:word.isupper=%s' % word1.isupper(),
            '+2:word.isdigit=%s' % word1.isdigit(),           
            '+2:postag=' + postag1,
            '+2:postag[:2]=' + postag1[:2],
            ])

    if i < (len(sent)-1):
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:word.isdigit=%s' % word1.isdigit(),           
            '+1:postag=' + postag1,
            '+1:postag[:2]=' + postag1[:2],
            ])
    else:
        features.append('EOS')

    return features

def sent2features_level_2(sent):
    return [word2features_level_2(sent, i) for i in range(len(sent))]

def sent2labels_level_2(sent):
    return [label for token, postag, label, pre_label in sent]

def sent2tokens_level_2(sent):
    return [token for token, postag, label, pre_label in sent] 