function api_call(input) {
    var url = 'http://localhost:5000/api';
    var data = input;

    fetch(url, {
    method: 'POST', // or 'PUT'
    body: data, // data can be `string` or {object}!
    headers: new Headers({
        'Content-Type': 'text/plain'
    })
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        result = response;
        var output_lv_1 = document.getElementById('output_lv_1');
        console.log(result);
        output_lv_1.innerHTML = "";
        var i;
        var lv_1 = result.result_lv_1;
        var sentence_lv_1 = "";
        for (i = 0; i < lv_1.length; i++) { 
            word_formated = lv_1[i][0].replace("_"," ");
            if(lv_1[i][2] != 'O'){
                if(lv_1[i][2].charAt(0)== 'B') word_formated = '<small class="axlabel product">' + word_formated;
                if( i == lv_1.length -1 || lv_1[i+1][2] === 'O') word_formated += '</small>';
            }
            sentence_lv_1 += word_formated + " ";
        }
        output_lv_1.innerHTML+= sentence_lv_1;

        var div_level_2 = "";
        var output_lv_2 = document.getElementById('output_lv_2');
        output_lv_2.innerHTML = "";
        var lv_2 = result.result_lv_2;
        for (i = 0; i < lv_2.length; i++){
            var sentence_lv_2 = "";
            var product = lv_2[i];
            for (j = 0; j < product.length; j++){
                word_formated = product[j][0].replace("_"," ");
                if(product[j][3] != 'O'){
                    var label = product[j][3].split("-")[1];
                    if(product[j][3].charAt(0)== 'B') {
                        word_formated = '<small class="axlabel ' + label + '">' + word_formated;

                    }
                    if( j == product.length -1 || product[j+1][3] === 'O' || product[j+1][3].charAt(0)== 'B') word_formated += '</small>';
                }
                sentence_lv_2 += word_formated + " ";
            }
            div_level_2 =  div_level_2 + '<div  class="message-body">' + sentence_lv_2 + '</div>';
            console.log(div_level_2);
        }
        output_lv_2.innerHTML = '<article class="message">' + div_level_2 + '</article>';

        $("#sentence").val("");
    });
}
$( document ).ready(function() {
    // request when clicking on the button
    $('#btn').click(function() {
        // get the input data
        var input = $("#sentence").val();
        api_call(input);
        
    });
});