#!/usr/bin/python
# -*- coding: utf8 -*-

""" CRF-Based Named Entity Recognition
    Use pyCRFsuite
    Author: Tran Thi Oanh
"""

# import pycrfsuite
import pycrfsuite
import numpy as np

from Feature_Extraction import *

class NER_recognizer:

    def __init__(self, **kwargs):
        self.config = {
            'language': 'vi',
            'crf_options': '',
        }
        self.remove_temp_file = True

        self.regex_dict = {}
        for key in self.config:
            setattr(self, key, self.config[key])

        config_file = kwargs.get('config_file')

        if config_file != None:
            self.load_config(config_file)

        if kwargs.get('model_file') != None:
            self.model_file = kwargs.get('model_file')

        for key in kwargs:
            setattr(self, key, kwargs[key])

   
    def crf_test(self, model_file, test_sents, conll_file_res):
        """
        testing the tagger by labelling for each sentence in test_sents
        :param model_file:  the model trained by using pycrf
        :param test_sents: all testing sentences
        :param conll_file_res: results of automatically labelling NER using the trained tagger stored in the model file
        :return: write results into conll_file_res
        """

        tagger = pycrfsuite.Tagger()
        tagger.open(model_file)

        outf = open(conll_file_res, "w")
        for example_sent in test_sents:
            # print(example_sent)
            predicted_labels = tagger.tag(sent2features(example_sent))
            correct_labels = sent2labels(example_sent)
            tokens = sent2tokens(example_sent)

            size = len(tokens)
            for i in range(size):
                outf.writelines(tokens[i] + " " + tokens[i] + " " + correct_labels[i] + " " + predicted_labels[i])
                outf.write("\n")

            outf.write("\n")

        outf.close()

        # Duyen Ha 

    def crf_test_level_1(self, model_file, test_sents, conll_file_res, test_sents_ls_level_2_origin):
        """
        testing the tagger by labelling for each sentence in test_sents
        :param model_file:  the model trained by using pycrf
        :param test_sents: all testing sentences
        :param conll_file_res: results of automatically labelling NER using the trained tagger stored in the model file
        :return: write results into conll_file_res
        """
        pre_label = []
        pre_label_buff = []
        tagger = pycrfsuite.Tagger()
        tagger.open(model_file)
        outf = open(conll_file_res, "w")
        for example_sent in test_sents:
            predicted_labels = tagger.tag(sent2features(example_sent))
            correct_labels = sent2labels(example_sent)
            tokens = sent2tokens(example_sent)
            # test_sents_ls_level_2= test_sents_ls_level_2_origin
            size = len(tokens)
            for i in range(size):
                outf.writelines(tokens[i] + " " + tokens[i] + " " + correct_labels[i] + " " + predicted_labels[i])
                outf.write("\n")
                pre_label_buff.append(predicted_labels[i])
                # test_sents_ls_level_2[sen_index][i].append(predicted_labels[i])
                # print(test_sents_ls_level_2[sen_index][i])
                # print("\n")
            # print("\n")
            pre_label.append(pre_label_buff)
            pre_label_buff=[]
            outf.write("\n")
        # print(pre_label)
        outf.close() 
        return pre_label
    
    def crf_test_level_2(self, model_file, test_sents, conll_file_res, pre_label):
        """
        testing the tagger by labelling for each sentence in test_sents
        :param model_file:  the model trained by using pycrf
        :param test_sents: all testing sentences
        :param conll_file_res: results of automatically labelling NER using the trained tagger stored in the model file
        :return: write results into conll_file_res
        """

        tagger = pycrfsuite.Tagger()
        tagger.open(model_file)

        outf = open(conll_file_res, "w")
        for i in range(len(test_sents)):
            example_sent = test_sents[i];

            for j in range(len(example_sent)):
                # print(example_sent[j])
                example_sent[j][3] = pre_label[i][j]
                # print("=>")
                # print(example_sent[j])
                # print("\n")
            predicted_labels = tagger.tag(sent2features_level_2(example_sent))
            correct_labels = sent2labels_level_2(example_sent)
            tokens = sent2tokens_level_2(example_sent)
            size = len(tokens)
            for i in range(size):
                outf.writelines(tokens[i] + " " + tokens[i] + " " + correct_labels[i] + " " + predicted_labels[i])
                outf.write("\n")

            outf.write("\n")

        outf.close()