
def posTag(inputSen):
    viPosTag = "./vitk-pos.jar"
    inputfile= "./posTag/inputFile.txt"
    output_xml_file = "./posTag/result.xml"

    with open(inputfile, 'w') as fo:
       fo.write(inputSen)

    comd = 'java -jar  %s %s %s' % (viPosTag, inputfile, output_xml_file)
    os.system(comd)

    return "ok"


