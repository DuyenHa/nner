import os, sys
import xml.etree.ElementTree
from collections import defaultdict
import codecs
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

def posTag(inputSen):
    viPosTag = "./vitk-pos.jar"
    inputfile= "./posTag/inputFile.txt"
    output_xml_file = "./posTag/result.xml"

    file = codecs.open(inputfile, "w", "utf-8")
    file.write(inputSen)
    file.close()
    # with open(inputfile, 'w', "utf-8") as fo:
    #    fo.write(inputSen)

    comd = 'java -jar  %s %s %s' % (viPosTag, inputfile, output_xml_file)
    os.system(comd)

    resultObject = xml.etree.ElementTree.parse(output_xml_file).getroot()
    # result = defaultdict(list)
    result = []
    for wordTag in resultObject[0].findall('w'):
        # word = dict()
        # word["word"] = wordTag.text
        # word["POS"] = wordTag.get('pos')
        # result["result"].append(word)
        result.append([wordTag.text,wordTag.get('pos')])
    return result