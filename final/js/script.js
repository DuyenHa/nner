(function () {
  var data =
      {
          "text" : "Cần Ship hàng từ cc 187 Nguyễn lương bằng đến 273 cổ Nhuế, phí Ship 30K, tiền hàng 1045k. Ship tầm 9-10h sáng mai, ai tiện có thể Ship trong tối nay cũng đc. Ai đi Dc Alo 0989094148 ",
          "entities" : [
              {
                  "entity" : "datetime_period",
                  "value" : "\"9-10h sáng mai\"",
                  "start" : 99,
                  "end" : 113,
                  "subentities" : [
                      {
                          "entity" : "start_time",
                          "value" : "\"9\"",
                          "start" : 0,
                          "end" : 1
                      },
                      {
                          "entity" : "end_time",
                          "value" : "\"10h\"",
                          "start" : 2,
                          "end" : 5
                      },
                      {
                          "entity" : "period_of_day",
                          "value" : "\"sáng\"",
                          "start" : 6,
                          "end" : 10
                      },
                      {
                          "entity" : "date",
                          "value" : "\"mai\"",
                          "start" : 11,
                          "end" : 14
                      }
                  ]
              },
              {
                  "entity" : "date_time",
                  "value" : "\"tối nay\"",
                  "start" : 141,
                  "end" : 148,
                  "subentities" : [
                      {
                          "entity" : "period_of_day",
                          "value" : "\"tối\"",
                          "start" : 0,
                          "end" : 3
                      },
                      {
                          "entity" : "date",
                          "value" : "\"nay\"",
                          "start" : 4,
                          "end" : 7
                      }
                  ]
              }
          ]
      }
  var colors = {
    day_of_week: 'red',
    suffix: 'grey',
    prefix: 'lightBlue',
    date_time: 'yellow',
    period_of_day: 'orange',
    time: 'brown',
    date: 'lightGrey',
    urgent: 'crimson'
  }
  var fullText = data.text
  data.entities.forEach(function (entity, idParent) {
    var listWord = entity.subentities
    if (idParent === 0) {
      appendHtmlContainer(`<div class="singleEntity">${fullText.slice(0, entity.start)}</div>`)
    }
    var listWordLength = listWord ? listWord.length : 0
    if (listWordLength) {
      appendHtmlContainer(`<div class="multiEntity"><div class="textBlock" id="text_${idParent}"></div><div class="wordBlock" id="words_${idParent}"></div></div>`)
      var text = document.getElementById(`text_${idParent}`)
      var words = document.getElementById(`words_${idParent}`)
      listWord.forEach(function (value, index) {
        text.innerHTML = text.innerHTML + renderText(value.value, idParent, index, colors[entity.entity])
        words.innerHTML = words.innerHTML + renderWord(value.value, idParent, index, colors[value.entity])
      })
    } else {
      appendHtmlContainer(`<div class="singleEntity" style="background: ${colors[entity.entity]};">${entity.value}</div> &nbsp;`)
      appendHtmlContainer(`<div class="singleEntity">&nbsp;</div>`)
    }

    var checkHtmlRendered = setInterval(function () {
      var words = document.querySelectorAll(`.word_${idParent}`)
      var texts = document.querySelectorAll(`.text_${idParent}`)
      if (listWordLength && words.length === listWordLength && texts.length === listWordLength) {
        clearInterval(checkHtmlRendered)
        document.getElementById(`words_${idParent}`).style.marginLeft = -40 * (listWordLength - 1) / 2 + 'px'
        listWord.forEach(function (value, index) {
          var textPosition = document.getElementById(`text_${idParent}_${index}`).getBoundingClientRect();
          var wordPosition = document.getElementById(`word_${idParent}_${index}`).getBoundingClientRect();
          document.body.appendChild(createLine(
            textPosition.x + textPosition.width / 2,
            textPosition.y,
            wordPosition.x + wordPosition.width / 2,
            wordPosition.y
          ))
        })
      }
    }, 10)
    if (data.entities[idParent + 1]) {
      appendHtmlContainer(`<div class="singleEntity">${fullText.slice(entity.end, data.entities[idParent + 1].start)}</div>`)
    } else {
      appendHtmlContainer(`<div class="singleEntity">${fullText.slice(entity.end, fullText.length)}</div>`)
    }
  })
})()