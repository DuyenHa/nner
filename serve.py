import pycrfsuite
from preprocessing import *
from Feature_Extraction import *

def get_model_api():
    """Returns lambda function for api"""
    model_lv_1 = "model_file/model_level_1"
    model_lv_2 = "model_file/model_level_2"

    tagger_lv_1 = pycrfsuite.Tagger()
    tagger_lv_1.open(model_lv_1)
    tagger_lv_2 = pycrfsuite.Tagger()
    tagger_lv_2.open(model_lv_2)

    def model_api(input_data):
        # posTag(input_data)
        print(input_data)
        sen_pos = posTag(input_data)
        predicted_labels_lv_1 = tagger_lv_1.tag(sent2features(sen_pos))
        for i in range(len(sen_pos)):
            sen_pos[i].append(predicted_labels_lv_1[i])
        
        predicted_labels_lv_2 = tagger_lv_2.tag(sent2features_level_2(sen_pos))
        result_lv_2 = []
        product_temp = []
        for i in range(len(sen_pos)):
            sen_pos[i].append(predicted_labels_lv_2[i])
        for i in range(len(sen_pos)):
            if sen_pos[i][2] == 'O':
                if len(product_temp) != 0:
                    result_lv_2.append(product_temp)
                    product_temp = []
            else:
                product_temp.append(sen_pos[i])
            if i == len(sen_pos)-1 :
                if len(product_temp) != 0:
                    result_lv_2.append(product_temp)

        result=dict()
        result["result_lv_1"]= sen_pos
        result["result_lv_2"]= result_lv_2
        return result
    return model_api